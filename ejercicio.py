#Crear una función llamada simular_semiparabólico que calcule las posesiones x,y de un objeto y retorne una lista con las tuplas calculadas para los intervalos de tiempo de 0.2 segundos

def simular_semiparabolico (Vo, Yo):
    lista_resultado=[]
    
    g=-9.8
    t=0
    y=0
    
    while y>=0:
        x=Vo*t
        y=Yo + (1/2)*(g*t**2)
        t=t+0.2
        lista_resultado.append((x,y))
    return lista_resultado
r=simular_semiparabolico(5,25)
print (r)