import math

def simulador (Vo, a):
    
    lista_resultados=[]
    g=9.8
    
    y=0
    
    t=0
    
    while y>=0:
        
        x=Vo* math.cos (a)*t
        y=Vo*math.sin(a)*t - (0.5)*g*t**2
        lista_resultados.append ((x,y))
        
        t=t+1
        
    return lista_resultados 

r=simulador (100,math.pi/8)

    

print(r)